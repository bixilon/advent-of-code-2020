package de.bixilon.adventofcode.year2020

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

object Util {
    val LINE_SEPARATOR = System.getProperty("line.separator")

    fun readResource(path: String): String {
        return readReader(BufferedReader(InputStreamReader(Util::class.java.getResourceAsStream(path))), true)
    }

    fun readReader(reader: BufferedReader, closeStream: Boolean): String {
        val stringBuilder = StringBuilder()
        var line: String?
        while (reader.readLine().also { line = it } != null) {
            stringBuilder.append(line)
            stringBuilder.append(LINE_SEPARATOR)
        }
        stringBuilder.deleteCharAt(stringBuilder.length - 1)
        if (closeStream) {
            reader.close()
        }
        return stringBuilder.toString()
    }
}
