package de.bixilon.adventofcode.year2020.instructions

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.LongAccumulator

class JumpInstruction(argument: Int) : Instruction(argument) {

    override fun execute(currentPointer: Int, accumulator: AtomicInteger): Int {
        super.execute(currentPointer, accumulator)
        return currentPointer + argument
    }
}
