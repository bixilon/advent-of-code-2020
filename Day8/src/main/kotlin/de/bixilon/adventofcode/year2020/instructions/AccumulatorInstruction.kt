package de.bixilon.adventofcode.year2020.instructions

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.LongAccumulator

class AccumulatorInstruction(argument: Int) : Instruction(argument) {

    override fun execute(currentPointer: Int, accumulator: AtomicInteger): Int {
        accumulator.set(accumulator.get() + argument)
        return super.execute(currentPointer, accumulator)
    }
}
