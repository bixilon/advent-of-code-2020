package de.bixilon.adventofcode.year2020

import de.bixilon.adventofcode.year2020.instructions.Instruction
import java.util.concurrent.atomic.AtomicInteger

object Day8 {
    @JvmStatic
    fun main(args: Array<String>) {
        println("Advent of Code 2020: Day 8: Handheld Halting")
        println("Reading resource program.txt")
        val data = Util.readResource("/program.txt").split(Util.LINE_SEPARATOR)

        println("Read ${data.size} lines of instructions")

        val instructions: MutableList<Instruction> = mutableListOf()

        for (line in data) {
            if (line.isBlank()) {
                continue
            }
            val split = line.split("\\s+".toRegex())
            instructions.add(Instruction.getInstruction(split[0], split[1].toInt()))
        }

        println("Parsed ${instructions.size} instructions")

        var currentInstruction = instructions[0]
        val accumulator = AtomicInteger(0)
        var currentPointer = 0
        while (!currentInstruction.executed) {
            currentPointer = currentInstruction.execute(currentPointer, accumulator)
            currentInstruction = instructions[currentPointer]
        }
        error("Aborted execution at ${currentPointer}, because of infinite loop danger. Our accumulator value was ${accumulator.get()}")
    }
}
