package de.bixilon.adventofcode.year2020.instructions

import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.LongAccumulator

class NoOperationInstruction(argument: Int) : Instruction(argument)
