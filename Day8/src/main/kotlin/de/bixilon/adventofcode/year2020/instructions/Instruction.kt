package de.bixilon.adventofcode.year2020.instructions

import java.lang.IllegalArgumentException
import java.util.concurrent.atomic.AtomicInteger

abstract class Instruction(val argument: Int) {
    var executed = false

    /**
     * returns Int Pointer of next instruction
     */
    open fun execute(currentPointer: Int, accumulator: AtomicInteger): Int {
        executed = true
        return currentPointer + 1
    }


    companion object {
        fun getInstruction(operation: String, argument: Int): Instruction {
            return when (operation) {
                "acc" -> AccumulatorInstruction(argument)
                "jmp" -> JumpInstruction(argument)
                "nop" -> NoOperationInstruction(argument)
                else -> throw IllegalArgumentException("Invalid operation: $operation")
            }
        }
    }
}
